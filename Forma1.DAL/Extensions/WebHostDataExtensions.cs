﻿using Forma1.DAL.Seed;
using Forma1.Web.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace Forma1.DAL.Extensions
{
    public static class WebHostDataExtensions
    {
        public static IWebHost SeedUser(this IWebHost host)
        {
            return host.Scoped<UserSeeder>((s, l) =>
s.GetRequiredService<UserSeeder>().Seed().GetAwaiter().GetResult(), "Seeding default user as needed");
        }

        public static IWebHost MigrateDatabase<TContext>(this IWebHost host)
                where TContext : DbContext
            => Scoped<Forma1DbContext>(host, (s, l) => s.GetRequiredService<TContext>().Database.Migrate(), "Migrating database");

        private static IWebHost Scoped<TLog>(this IWebHost host,
                Action<IServiceProvider, ILogger<TLog>> action, string title)
        {
            using (var scope = host.Services.CreateScope())
            {
                var serviceProvider = scope.ServiceProvider;
                var logger = serviceProvider.GetRequiredService<ILogger<TLog>>();
                try
                {
                    action(serviceProvider, logger);
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, $"An error occurred during action: {title}");
                }
            }
            return host;
        }
    }
}

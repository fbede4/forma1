﻿using Forma1.Web.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forma1.DAL.Seed
{
    public class UserSeeder
    {
        public UserSeeder(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }
        public UserManager<ApplicationUser> UserManager { get; }
        public async Task Seed()
        {
            if (await UserManager.FindByNameAsync("admin") == null)
            {
                var user = new ApplicationUser
                {
                    Email = "admin",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserName = "admin",
                };
                var createResult = await UserManager.CreateAsync(user, "f1test2018");
                if (!createResult.Succeeded)
                    throw new ApplicationException($"Default user could not be created: " +
                        $"{string.Join(", ", createResult.Errors.Select(e => e.Description))}");
            }
        }
    }
}

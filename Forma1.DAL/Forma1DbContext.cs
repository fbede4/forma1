﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Forma1.Web.Models;
using Forma1.DAL.Entities;

namespace Forma1.Web.Data
{
    public class Forma1DbContext : IdentityDbContext<ApplicationUser>
    {
        public Forma1DbContext(DbContextOptions<Forma1DbContext> options)
            : base(options)
        {
        }

        public DbSet<Team> Teams { get; set; }
    }
}

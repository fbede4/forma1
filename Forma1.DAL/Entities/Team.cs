﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Forma1.DAL.Entities
{
    public class Team
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Display(Name = "Year Of Establishment")]
        [Required]
        public int YearOfEstablishment { get; set; }
        [Display(Name = "Number of World Champions won")]
        [Required]
        public int ChampionsWon { get; set; }
        [Display(Name = "Is Currently a Member")]
        [Required]
        public bool PaidMembership { get; set; }
    }
}
